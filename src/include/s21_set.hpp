#ifndef SRC_INCLUDE_S21_SET_HPP_
#define SRC_INCLUDE_S21_SET_HPP_

#include <iostream>

#include "RBTree.hpp"

namespace s21 {

template <class T>
class set : public RBTree<T> {
        friend class iterator;

 public:
        using iterator = typename RBTree<T>::iterator;

        set() : RBTree<T>() {}
        set(std::initializer_list<T> const &items) : RBTree<T>(items) {}
        set(const set<T> &other) : RBTree<T>(other) {}
        set(set<T> &&other) : RBTree<T>(other) {}

        std::pair<iterator, bool> insert(const T &value) {
            iterator it = RBTree<T>::end();
            bool read = false;
            if (!RBTree<T>::contains(value)) {
                it = RBTree<T>::insert(value);
                read = true;
            }
            return {it, read};
        }

        void merge(set<T> &other) {
            set<T> temp;
            for (auto it = other.begin(); it != other.end(); ++it) {
                auto it_in = insert(*it);
                if (!it_in.second) {
                    temp.insert(*it);
                }
            }
            other = std::move(temp);
        }

        set<T> &operator=(const set &other) {
            RBTree<T>::operator=(other);
            return *this;
        }

        set<T> &operator=(set &&other) {
            RBTree<T>::operator=(other);
            return *this;
        }

        template <class... Args>
        std::pair<iterator, bool> emplace(Args &&...args) {
            return (insert(args), ...);
        }

        std::pair<iterator, bool> emplace(void) = delete;

 private:
        using RBTree<T>::equal_range;
        using RBTree<T>::lower_bound;
        using RBTree<T>::upper_bound;
        using RBTree<T>::count;
};
}  // namespace s21

#endif  // SRC_INCLUDE_S21_SET_HPP_
