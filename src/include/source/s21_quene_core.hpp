// "Copyright [year] <Copyright Owner>"
#ifndef SRC_INCLUDE_SOURCE_S21_QUENE_CORE_HPP_
#define SRC_INCLUDE_SOURCE_S21_QUENE_CORE_HPP_

#include <cstddef>
#include <iostream>

namespace s21 {
template <typename T>
class quene_core {
 public:
    using value_type = T;
    using reference = T&;
    using const_reference = const T&;
    using size_type = size_t;
    quene_core() : head(nullptr), tail(nullptr) {}
    ~quene_core();

    bool empty();
    size_type size();

    virtual void push(const_reference value) = 0;

    void pop();
    void swap(quene_core& other);

 protected:
    struct node {
        struct node* next;
        struct node* prev;
        value_type data;
    };
    node* head;
    node* tail;
    void quene_core_init(std::initializer_list<value_type> const& items);
    void remove_quene_core();
    void create_top_node();
    void create_tail_node();
    void move(quene_core&& s);
};
    ///////////////// TTP  /////////////

    template <typename T>
    void quene_core<T>::create_top_node() {
        node* new_head = new node;
        new_head->prev = head;
        new_head->next = nullptr;
        if (head != nullptr) {
            head->next = new_head;
        }
        head = new_head;
        if (tail == nullptr) {
            tail = head;
        }
    }

    template <typename T>
    void quene_core<T>::create_tail_node() {
        node* new_tail = new node;
        new_tail->next = tail;
        new_tail->prev = nullptr;
        if (tail != nullptr) {
            tail->prev = new_tail;
        }
        tail = new_tail;
        if (head == nullptr) {
            head = tail;
        }
    }

    template <typename T>
    void quene_core<T>::quene_core_init(std::initializer_list<value_type> const& items) {
        auto iter = items.begin();
        while (iter != items.end()) {
            push(*iter);
            ++iter;
        }
    }

    template <typename T>
    bool quene_core<T>::empty() {
        if (head == nullptr) {
            return true;
        } else {
            return false;
        }
    }

    template <typename T>
    typename quene_core<T>::size_type quene_core<T>::size() {
        size_type count = 0;
        node* tmp = head;
        if (tmp != nullptr) {
            ++count;
            while (tmp->prev != nullptr) {
                ++count;
                tmp = tmp->prev;
            }
        }
        return count;
    }

    template <typename T>
    void quene_core<T>::pop() {
        if (head != nullptr) {
            if (head->prev != nullptr) {
                node* tmp = head;
                head = tmp->prev;
                head->next = nullptr;
                delete tmp;
            } else {
                if (tail == head) {
                    tail = nullptr;
                }
                delete head;
                head = nullptr;
            }
        }
    }

    template <typename T>
    void quene_core<T>::remove_quene_core() {
        while (!empty()) {
            pop();
        }
        head = nullptr;
        tail = nullptr;
    }

    template <typename T>
    quene_core<T>::~quene_core() {
        remove_quene_core();
    }

    template <typename T>
    void s21::quene_core<T>::swap(quene_core& other) {
        node* tmp_head = head;
        node* tmp_tail = tail;
        head = other.head;
        tail = other.tail;
        other.head = tmp_head;
        other.tail = tmp_tail;
    }

    template <typename T>
    void s21::quene_core<T>::move(quene_core&& s) {
        remove_quene_core();
        head = s.head;
        s.head = nullptr;
        tail = s.tail;
        s.tail = nullptr;
    }

}  //  namespace s21

#endif  //  SRC_INCLUDE_SOURCE_S21_QUENE_CORE_HPP_
