#include "s21_vector.hpp"

namespace s21 {

    template <typename T>
    vector<T>::vector() : _array(nullptr), _size(0), _capacity(0) {}

    template <typename T>
    vector<T>::vector(size_type n) try : _array(new T[n]), _size(n), _capacity(n) {
        for (size_type i = 0; i < _size; i++) _array[i] = 0;
        std::cout << std::endl;
    } catch (int& e) {
        if (_array)
            delete[] _array;
        e = 1;
        throw std::invalid_argument("vector(n), exception");
    }

    template <typename T>
    vector<T>::vector(std::initializer_list<value_type> const& items) : vector(items.size()) {
        std::copy(items.begin(), items.end(), begin());
    }

    template <typename T>
    vector<T>::vector(const vector& v) {
        _size = v._size;
        _capacity = v._size;
        if (v.begin()) {
            _array = new T[_size];
            std::copy(v.begin(), v.end(), begin());
        } else {
            _array = nullptr;
        }
    }

    template <typename T>
    vector<T>::vector(vector&& v) : _array(v._array), _size(v._size), _capacity(v._capacity) {
        v._array = nullptr;
    }

    template <typename T>
    void vector<T>::destroy() {
        if (_array)
            delete[] _array;
    }

    template <typename T>
    vector<T>::~vector() {
        destroy();
    }

    template <typename T>
    vector<T>& vector<T>::operator=(vector&& v) {
        destroy();
        _array = v._array;
        _size = v._size;
        _capacity = v._capacity;
        v._array = nullptr;
        return *this;
    }

    template <typename T>
    vector<T>& vector<T>::operator=(const vector& v) {
        auto temp_v = v;
        if (temp_v.size() > size()) {
            if (temp_v.capacity() > _capacity)
                _capacity = temp_v.capacity();
            value_type* new_array = new value_type[temp_v.size()];
            std::copy(temp_v.begin(), temp_v.end(), new_array);
            delete[] _array;
            _array = new_array;
        } else {
            _size = temp_v.size();
            std::copy(temp_v.begin(), temp_v.end(), begin());
        }
        _size = temp_v.size();
        return *this;
    }

    template <typename T>
    typename vector<T>::reference vector<T>::at(size_type pos) {
        if (pos >= _size) {
            throw std::out_of_range("n >= size");
        }
        return _array[pos];
    }

    template <typename T>
    typename vector<T>::reference vector<T>::operator[](size_type pos) {
        return _array[pos];
    }

    template <typename T>
    typename vector<T>::const_reference vector<T>::front() const {
        return _array[0];
    }

    template <typename T>
    typename vector<T>::reference vector<T>::front() {
        return _array[0];
    }

    template <typename T>
    typename vector<T>::const_reference vector<T>::back() const {
        return _array[_size - 1];
    }

    template <typename T>
    typename vector<T>::reference vector<T>::back() {
        return _array[_size - 1];
    }

    template <typename T>
    typename vector<T>::iterator vector<T>::data() {
        return _array;
    }

    template <typename T>
    typename vector<T>::iterator vector<T>::begin() {
        return _array;
    }

    template <typename T>
    typename vector<T>::const_iterator vector<T>::begin() const {
        return _array;
    }

    template <typename T>
    typename vector<T>::iterator vector<T>::end() {
        return _array + _size;
    }

    template <typename T>
    typename vector<T>::const_iterator vector<T>::end() const {
        return _array + _size;
    }

    template <typename T>
    bool vector<T>::empty() {
        return _size == 0;
    }

    template <typename T>
    typename vector<T>::size_type vector<T>::size() {
        return _size;
    }

    template <typename T>
    typename vector<T>::size_type vector<T>::capacity() {
        return _capacity;
    }

    template <typename T>
    template <typename iterator_type>
    typename vector<T>::iterator vector<T>::insert(iterator_type pos,
                                                   const_reference value) {
        T* new_array = nullptr;
        if (size() + 1 > capacity()) {
            new_array = new T[_size + 1];
        } else {
            new_array = begin();
        }
        auto nai = new_array;
        auto i = begin();
        for (; i != pos; i++, nai++) {
            *nai = *i;
        }
        *nai = value;
        T* res = nai;
        nai++;
        for (; i != end(); i++, nai++) {
            *nai = *i;
        }
        if (new_array != begin()) {
            delete[] _array;
            _array = new_array;
        }
        _size++;
        if (_capacity < _size)
            _capacity = _size;
        return res;
    }

    template <typename T>
    void vector<T>::reserve(size_type size) {
        if (capacity() < size) {
            _capacity = size;
            T* new_array = new T[_capacity];
            std::copy(begin(), end(), new_array);
            delete[] _array;
            _array = new_array;
        }
    }

    template <typename T>
    void vector<T>::shrink_to_fit() {
        if (size() != capacity()) {
            _capacity = _size;
            T* new_array = new T[_size];
            std::copy(begin(), end(), new_array);
            delete[] _array;
            _array = new_array;
        }
    }

    template <typename T>
    void vector<T>::clear() {
        _size = 0;
    }

    template <typename T>
    void vector<T>::erase(iterator pos) {
        T* new_array = new T[_capacity];
        auto nai = new_array;
        auto i = begin();
        for (; i != pos; i++, nai++) {
            *nai = *i;
        }
        i++;
        for (; i != end(); i++, nai++) {
            *nai = *i;
        }
        delete[] _array;
        _array = new_array;
        _size--;
    }

    template <typename T>
    void vector<T>::push_back(const_reference value) {
        insert(end(), value);
    }

    template <typename T>
    void vector<T>::pop_back() {
        _size--;
    }

    template <typename T>
    void vector<T>::swap(vector& other) {
        auto size_temp = _size;
        auto capacity_temp = _capacity;
        auto array_temp = _array;
        _size = other.size();
        _capacity = other.capacity();
        _array = other.begin();
        other._capacity = capacity_temp;
        other._size = size_temp;
        other._array = array_temp;
    }

    template <typename T>
    typename vector<T>::size_type vector<T>::max_size() {
        return std::numeric_limits<size_type>::max() / sizeof(value_type) / 2;
    }

    template <typename T>
    template <typename... Args>
    typename vector<T>::iterator vector<T>::emplace(const_iterator pos, Args&&... args) {
        vector<T> arr = {args...};
        auto local_pos = pos;
        for (auto i = arr.end() - 1; i != arr.begin() - 1; i--) {
            local_pos = insert(local_pos, *i);
        }
        return const_cast<iterator>(local_pos);
    }

    template <typename T>
    template <typename... Args>
    void vector<T>::emplace_back(Args&&... args) {
        (push_back(args), ...);
    }

}  // namespace s21
