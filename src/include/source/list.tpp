#include "s21_list.hpp"
namespace s21 {

    template <typename T>
    list<T>::list() : _head(nullptr), _tail(nullptr), _size(0) {
        create_tail();
    }

    template <typename T>
    list<T>::list(size_type n) : list() {
        for (size_type i = 0; i < n; i++) {
            push_back(0);
        }
    }

    template <typename T>
    list<T>::list(std::initializer_list<value_type> const& items) : list(items.size()) {
        auto temp_items = items;
        for (auto i = temp_items.begin(), j = begin(); i != temp_items.end(); ++i, ++j) *j = *i;
    }

    template <typename T>
    list<T>::list(const list& l) : list(l.size()) {
        auto temp_node = _head;
        auto temp_l_node = l._head;
        for (size_type i = 0; i < _size; i++) {
            temp_node->_data = temp_l_node->_data;
            temp_node = temp_node->_next;
            temp_l_node = temp_l_node->_next;
        }
    }

    template <typename T>
    void list<T>::assign(list l) {
        _head = l._head;
        _tail = l._tail;
        _size = l._size;
        l._head = nullptr;
        l._tail = nullptr;
        l._size = 0;
    }

    template <typename T>
    list<T>::list(list&& l) {
        assign(l);
    }

    template <typename T>
    list<T>::~list() {
        clear();
    }

    template <typename T>
    typename list<T>::list_node* list<T>::node_from_iter(iterator pos) {
        auto temp_node = _head;
        auto temp_iter = begin();
        while (temp_iter != pos) {
            temp_iter++;
            temp_node = temp_node->_next;
        }
        return temp_node;
    }

    template <typename T>
    template <typename iterator_type>
    typename list<T>::iterator list<T>::insert(iterator_type pos, const_reference value) {
        _size++;
        auto temp_node = node_from_iter(pos);
        auto new_node = new list_node();
        if (temp_node == _head)
            _head = new_node;
        new_node->_data = value;
        new_node->_prev = temp_node->_prev;
        new_node->_next = temp_node;
        temp_node->_prev->_next = new_node;
        temp_node->_prev = new_node;
        return iterator(new_node);
    }

    template <typename T>
    list<T>& list<T>::operator=(list&& l) {
        clear();
        assign(l);
        return *this;
    }

    template <typename T>
    list<T>& list<T>::operator=(const list& l) {
        clear();
        create_tail();
        auto temp_l = l;
        for (auto i = temp_l.begin(); i != temp_l.end(); i++) push_back(*i);
        return *this;
    }

    template <typename T>
    void list<T>::create_tail() {
        _tail = new list_node();
        _head = _tail;
        _tail->_next = _head;
        _tail->_prev = _head;
    }

    template <typename T>
    void list<T>::push_back(const_reference value) {
        insert(end(), value);
    }

    template <typename T>
    typename list<T>::iterator list<T>::begin() {
        iterator iter = iterator(this->_head);
        return iter;
    }

    template <typename T>
    typename list<T>::iterator list<T>::end() {
        iterator iter = iterator(this->_tail);
        return iter;
    }

    template <typename T>
    void list<T>::clear() {
        if (_head != nullptr && _tail != nullptr) {
            auto i = _head;
            auto this_size = size();
            for (size_t it = 0; it <= this_size; it++) {
                auto j = i;
                if (it < this_size)
                    i = i->_next;
                delete j;
            }
            _head = _tail = nullptr;
            _size = 0;
        }
    }

    template <typename T>
    typename list<T>::const_iterator list<T>::begin() const {
        const_iterator iter = const_iterator(this->_head);
        return iter;
    }

    template <typename T>
    typename list<T>::const_iterator list<T>::end() const {
        const_iterator iter = const_iterator(this->_tail);
        return iter;
    }

    template <typename T>
    typename list<T>::size_type list<T>::size() const {
        return _size;
    }

    template <typename T>
    typename list<T>::const_reference list<T>::front() const {
        return *(begin());
    }

    template <typename T>
    typename list<T>::reference list<T>::front() {
        return *(begin());
    }

    template <typename T>
    typename list<T>::const_reference list<T>::back() const {
        return *(--end());
    }

    template <typename T>
    typename list<T>::reference list<T>::back() {
        return *(--end());
    }

    template <typename T>
    bool list<T>::empty() {
        return _size == 0;
    }

    template <typename T>
    typename list<T>::size_type list<T>::max_size() {
        return std::numeric_limits<size_type>::max() / 48;
    }

    template <typename T>
    void list<T>::erase(iterator pos) {
        _size--;
        auto temp_node = node_from_iter(pos);
        if (temp_node == _head)
            _head = temp_node->_next;
        temp_node->_prev->_next = temp_node->_next;
        temp_node->_next->_prev = temp_node->_prev;
        delete temp_node;
    }

    template <typename T>
    void list<T>::pop_back() {
        erase(--end());
    }

    template <typename T>
    void list<T>::push_front(const_reference value) {
        insert(begin(), value);
    }

    template <typename T>
    void list<T>::pop_front() {
        erase(begin());
    }

    template <typename T>
    void list<T>::swap(list& other) {
        auto temp_size = _size;
        auto temp_head = _head;
        auto temp_tail = _tail;
        _size = other._size;
        _head = other._head;
        _tail = other._tail;
        other._size = temp_size;
        other._head = temp_head;
        other._tail = temp_tail;
    }

    template <typename T>
    void list<T>::listnode_swap(list_node* a, list_node* b) {
        int tmp = a->_data;
        a->_data = b->_data;
        b->_data = tmp;
    }

    template <typename T>
    void list<T>::listnode_qsort(list_node* left, list_node* right) {
        if (left == right) {
            return;
        } else if (left->_next == right) {
            if (left->_data > right->_data)
                listnode_swap(left, right);
        } else {
            list_node* last = left;
            list_node* current = left;
            do {
                current = current->_next;
                if (current->_data < left->_data) {
                    last = last->_next;
                    listnode_swap(last, current);
                }
            } while (current != right);
            listnode_swap(left, last);
            listnode_qsort(left, last);
            if (last != right)
                listnode_qsort(last->_next, right);
        }
    }

    template <typename T>
    void list<T>::sort() {
        listnode_qsort(_head, _tail->_prev);
    }

    template <typename T>
    void list<T>::merge_insert(list_node* current_node, list_node* other_node, list& other) {
        auto tmp = other_node->_next;
        current_node->_prev->_next = other_node;
        other_node->_prev = current_node->_prev;
        current_node->_prev = other_node;
        other_node->_next = current_node;
        if (other_node == other._head)
            other._head = tmp;
        other._size--;
        _size++;
        if (current_node == _head)
            _head = other_node;
    }

    template <typename T>
    void list<T>::merge(list& other) {
        auto other_node = other._head;
        auto current_node = _head;
        while ((current_node != _tail) && (other_node != other._tail)) {
            if (other_node->_data <= current_node->_data) {
                auto tmp = other_node->_next;
                merge_insert(current_node, other_node, other);
                other_node = tmp;
            } else {
                current_node = current_node->_next;
            }
        }
        while (other_node != other._tail) {
            merge_insert(_tail, other_node, other);
        }
    }

    template <typename T>
    void list<T>::splice(const_iterator pos, list& other) {
        auto other_node = other._head;
        auto current_node = pos.get_node()->_next;
        while (other_node != other._tail) {
            auto tmp = other_node->_next;
            merge_insert(current_node, other_node, other);
            other_node = tmp;
        }
    }

    template <typename T>
    void list<T>::reverse() {
        auto tmp_node = _head;
        _head = _tail->_prev;
        while (tmp_node != _tail) {
            auto tmp_next = tmp_node->_next;
            auto tmp_prev = tmp_node->_prev;
            tmp_node->_prev = tmp_node->_next;
            tmp_node->_next = tmp_prev;
            tmp_node = tmp_next;
        }
        _tail->_next = _head;
    }

    template <typename T>
    void list<T>::unique() {
        auto tmp_node = _head;
        while (tmp_node != _tail) {
            if (tmp_node->_data == tmp_node->_next->_data) {
                erase(iterator(tmp_node->_next));
            } else {
                tmp_node = tmp_node->_next;
            }
        }
    }

    template <typename T>
    typename list<T>::iterator list<T>::iter_convert(const const_iterator pos) {
        list_node* node = pos.get_node();
        iterator res(node);
        return res;
    }

    template <typename T>
    typename list<T>::const_iterator list<T>::iter_convert(iterator pos) const {
        list_node* node = pos.get_node();
        iterator res(node);
        return res;
    }

    template <typename T>
    template <typename... Args>
    typename list<T>::iterator list<T>::emplace(const_iterator pos, Args&&... args) {
        list<T> arr = {args...};
        iterator local_pos = iter_convert(pos);
        for (auto i = --arr.end(); i != arr.end(); i--) {
            local_pos = insert(local_pos, *i);
        }
        return local_pos;
    }

    template <typename T>
    template <typename... Args>
    void list<T>::emplace_back(Args&&... args) {
        (push_back(args), ...);
    }

    template <typename T>
    template <typename... Args>
    void list<T>::emplace_front(Args&&... args) {
        list<T> arr = {args...};
        for (auto i = --arr.end(); i != arr.end(); i--) {
            push_front(*i);
        }
    }

}  // namespace s21
