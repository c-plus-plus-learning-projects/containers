#include "s21_list.hpp"

namespace s21 {
    template <typename T>
    list<T>::ListConstIterator::ListConstIterator() : _current_node(nullptr) {}

    template <typename T>
    list<T>::ListConstIterator::ListConstIterator(list<T>::list_node* list_node) : _current_node(list_node) {}

    template <typename T>
    typename list<T>::value_type& list<T>::ListConstIterator::operator*() const {
        return _current_node->_data;
    }

    template <typename T>
    bool list<T>::ListConstIterator::operator==(ListConstIterator other) {
        return _current_node == other._current_node;
    }

    template <typename T>
    bool list<T>::ListConstIterator::operator!=(ListConstIterator other) {
        return _current_node != other._current_node;
    }

    template <typename T>
    typename list<T>::list_node* list<T>::ListConstIterator::get_node() const {
        return _current_node;
    }

}  // namespace s21
