#include "s21_list.hpp"

namespace s21 {

    template <typename T>
    list<T>::ListIterator::ListIterator() : _current_node(nullptr) {}

    template <typename T>
    list<T>::ListIterator::ListIterator(list<T>::list_node* list_node) : _current_node(list_node) {}

    template <typename T>
    typename list<T>::value_type& list<T>::ListIterator::operator*() {
        return _current_node->_data;
    }

    template <typename T>
    typename list<T>::ListIterator list<T>::ListIterator::operator++() {
        _current_node = _current_node->_next;
        return *this;
    }

    template <typename T>
    typename list<T>::ListIterator list<T>::ListIterator::operator++(int) {
        auto temp_node = this;
        _current_node = _current_node->_next;
        return *temp_node;
    }

    template <typename T>
    typename list<T>::ListIterator list<T>::ListIterator::operator--() {
        _current_node = _current_node->_prev;
        return *this;
    }

    template <typename T>
    typename list<T>::ListIterator list<T>::ListIterator::operator--(int) {
        auto temp_node = this;
        _current_node = _current_node->_prev;
        return *temp_node;
    }

    template <typename T>
    bool list<T>::ListIterator::operator==(ListIterator other) {
        return _current_node == other._current_node;
    }

    template <typename T>
    bool list<T>::ListIterator::operator!=(ListIterator other) {
        return _current_node != other._current_node;
    }

}  // namespace s21
