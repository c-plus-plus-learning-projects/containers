#ifndef SRC_INCLUDE_S21_MAP_HPP_
#define SRC_INCLUDE_S21_MAP_HPP_

#include <iostream>

#include "s21_set.hpp"

namespace s21 {

template <class Key_, class Val_>
class map : public set<std::pair<const Key_, Val_>> {
 public:
        using key_type = Key_;
        using mapped_type = Val_;
        using value_type = std::pair<const key_type, mapped_type>;
        using reference = value_type &;
        using const_reference = const value_type &;
        using iterator = typename set<std::pair<const Key_, Val_>>::iterator;
        using size_type = size_t;

        map() : set<value_type>() {}
        map(std::initializer_list<value_type> const &items) : set<value_type>(items) {}
        map(const map<key_type, mapped_type> &other) : set<value_type>(other) {}
        map(map<key_type, mapped_type> &&other) : set<value_type>(other) {}

        map<key_type, mapped_type> &operator=(const map &other) {
            set<value_type>::operator=(other);
            return *this;
        }

        map<key_type, mapped_type> &operator=(map &&other) {
            set<value_type>::operator=(other);
            return *this;
        }

        mapped_type &operator[](key_type Key) {
            auto value = find(Key);

            if (value == map::end()) {
                map::insert(std::pair<key_type, mapped_type>(Key, 0));
                value = find(Key);
            }

            return (mapped_type &)value->second;
        }

        map<key_type, mapped_type>::iterator find(key_type Key) {
            auto it = map::begin();
            for (; it != map::end(); it++)
                if (it->first == Key)
                    break;
            return it;
        }

        std::pair<iterator, bool> insert_or_assign(value_type pair) {
            auto value = find(pair.first);
            bool status;

            if (value == map::end()) {
                map::insert(pair);
                status = 1;
                value = find(pair.first);
            } else {
                (mapped_type &)value->second = pair.second;
                status = 0;
            }

            return {value, status};
        }

        std::pair<iterator, bool> insert_or_assign(Key_ &&k, Val_ &&obj) {
            return insert_or_assign({k, obj});
        }

        bool contains(const key_type &Key) {
            return find(Key) != map::end();
        }

        template <class... Args>
        std::pair<iterator, bool> emplace(Args &&...args) {
            return (map::insert_or_assign(args), ...);
        }

        std::pair<iterator, bool> emplace(void) = delete;
};
}  // namespace s21

#endif  // SRC_INCLUDE_S21_MAP_HPP_
