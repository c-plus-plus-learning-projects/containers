#ifndef SRC_INCLUDE_S21_LIST_HPP_
#define SRC_INCLUDE_S21_LIST_HPP_

#include <stdarg.h>

#include <iostream>

namespace s21 {

template <typename T>
class list {
 private:
        typedef T value_type;
        typedef T& reference;
        typedef const T& const_reference;

        struct list_node {
            list_node* _prev;
            list_node* _next;
            value_type _data;
        };

        class ListIterator {
         private:
            list_node* _current_node;

         public:
            ListIterator();
            explicit ListIterator(list<T>::list_node* list);
            value_type& operator*();
            ListIterator operator++();
            ListIterator operator++(int);
            ListIterator operator--();
            ListIterator operator--(int);
            bool operator==(ListIterator other);
            bool operator!=(ListIterator other);
            list_node* get_node() const;
        };

        class ListConstIterator {
         private:
            list_node* _current_node;

         public:
            ListConstIterator();
            explicit ListConstIterator(list<T>::list_node* list);
            value_type& operator*() const;
            bool operator==(ListConstIterator other);
            bool operator!=(ListConstIterator other);
            list_node* get_node() const;
        };

        typedef size_t size_type;

        list_node* _head;
        list_node* _tail;
        size_type _size;

        void create_tail();
        void assign(list l);
        list_node* node_from_iter(ListIterator pos);
        void listnode_swap(list_node* a, list_node* b);
        void listnode_qsort(list_node* left, list_node* right);
        void merge_insert(list_node* current_node, list_node* other_node, list& other);
        ListIterator const_convert(ListConstIterator pos);
        ListIterator iter_convert(const ListConstIterator pos);
        ListConstIterator iter_convert(ListIterator pos) const;

 public:
        typedef ListIterator iterator;
        typedef ListConstIterator const_iterator;
        list();
        explicit list(size_type n);
        list(std::initializer_list<value_type> const& items);
        list(const list& l);
        list(list&& l);
        ~list();
        list& operator=(list&& l);
        list& operator=(const list& l);

        const_reference front() const;
        reference front();
        const_reference back() const;
        reference back();

        const_iterator begin() const;
        iterator begin();
        const_iterator end() const;
        iterator end();

        bool empty();
        size_type size() const;
        size_type max_size();

        void clear();

        template <typename iterator_type>
        iterator insert(iterator_type pos, const_reference value);

        void erase(iterator pos);
        void push_back(const_reference value);
        void pop_back();
        void push_front(const_reference value);
        void pop_front();
        void swap(list& other);
        void merge(list& other);
        void splice(const_iterator pos, list& other);
        void reverse();
        void unique();
        void sort();

        template <typename... Args>
        iterator emplace(const_iterator pos, Args&&... args);

        template <typename... Args>
        void emplace_back(Args&&... args);

        template <typename... Args>
        void emplace_front(Args&&... args);
};

}  // namespace s21

#include "list.tpp"
#include "list_const_iterator.tpp"
#include "list_iterator.tpp"

#endif  // SRC_INCLUDE_S21_LIST_HPP_
