#ifndef SRC_INCLUDE_S21_ARRAY_HPP_
#define SRC_INCLUDE_S21_ARRAY_HPP_

#include <iostream>

namespace s21 {

template <typename T, std::size_t N>
class array {
        using value_type = T;
        using reference = T &;
        using const_reference = const T &;
        using iterator = T *;
        using const_iterator = const T *;
        using size_type = std::size_t;

 private:
        value_type *_data;
        size_type _size;

 public:
        array();
        explicit array(std::initializer_list<value_type> const &items);
        array(const array &arr);
        ~array();

        iterator begin();
        const_iterator begin() const;
        iterator end();
        const_iterator end() const;

        reference at(size_type pos);
        reference operator[](size_type pos);
        reference front();
        const_reference front() const;
        reference back();
        const_reference back() const;
        iterator data();

        bool empty();
        size_type size();
        size_type max_size();

        array<T, N> &operator=(const array &arr);

        void swap(array &arr);
        void fill(const_reference value);
};

    template <typename T, std::size_t N>
    array<T, N>::array() : _data(new T[N]), _size(N) {}

    template <typename T, std::size_t N>
    array<T, N>::array(std::initializer_list<value_type> const &items) : array() {
        if (items.size() > _size)
            throw std::out_of_range("Too many arguments for array");
        std::copy(items.begin(), items.end(), begin());
    }

    template <typename T, std::size_t N>
    array<T, N>::array(const array &arr) {
        _size = arr._size;
        if (arr.begin()) {
            _data = new T[_size];
            std::copy(arr.begin(), arr.end(), begin());
        } else {
            _data = nullptr;
        }
    }

    template <typename T, std::size_t N>
    array<T, N>::~array() {
        if (_data)
            delete[] _data;
        _size = 0;
    }

    template <typename T, std::size_t N>
    typename array<T, N>::iterator array<T, N>::begin() {
        return _data;
    }

    template <typename T, std::size_t N>
    typename array<T, N>::const_iterator array<T, N>::begin() const {
        return _data;
    }

    template <typename T, std::size_t N>
    typename array<T, N>::iterator array<T, N>::end() {
        return _data + _size;
    }

    template <typename T, std::size_t N>
    typename array<T, N>::const_iterator array<T, N>::end() const {
        return _data + _size;
    }

    template <typename T, std::size_t N>
    typename array<T, N>::reference array<T, N>::at(size_type pos) {
        if (pos >= _size)
            throw std::out_of_range("Out of array range");
        return _data[pos];
    }

    template <typename T, std::size_t N>
    typename array<T, N>::reference array<T, N>::operator[](size_type pos) {
        return _data[pos];
    }

    template <typename T, std::size_t N>
    typename array<T, N>::reference array<T, N>::front() {
        return _data[0];
    }
    template <typename T, std::size_t N>
    typename array<T, N>::const_reference array<T, N>::front() const {
        return _data[0];
    }

    template <typename T, std::size_t N>
    typename array<T, N>::reference array<T, N>::back() {
        return _data[_size - 1];
    }

    template <typename T, std::size_t N>
    typename array<T, N>::const_reference array<T, N>::back() const {
        return _data[_size - 1];
    }

    template <typename T, std::size_t N>
    typename array<T, N>::iterator array<T, N>::data() {
        return _data;
    }

    template <typename T, std::size_t N>
    bool array<T, N>::empty() {
        return _size == 0;
    }

    template <typename T, std::size_t N>
    typename array<T, N>::size_type array<T, N>::size() {
        return _size;
    }

    template <typename T, std::size_t N>
    typename array<T, N>::size_type array<T, N>::max_size() {
        return _size;
    }

    template <typename T, std::size_t N>
    array<T, N> &array<T, N>::operator=(const array &arr) {
        _size = arr._size;
        if (arr.begin()) {
            delete[] _data;
            _data = new T[_size];
            for (size_type i = 0; i < _size; ++i)
                _data[i] = arr._data[i];
        } else {
            _data = nullptr;
        }
        return *this;
    }

    template <typename T, std::size_t N>
    void array<T, N>::swap(array &arr) {
        value_type *buf = _data;
        _data = arr._data;
        arr._data = buf;
    }

    template <typename T, std::size_t N>
    void array<T, N>::fill(const_reference value) {
        for (size_type i = 0; i < _size; ++i)
            _data[i] = value;
    }
}  // namespace s21

#endif  // SRC_INCLUDE_S21_ARRAY_HPP_
