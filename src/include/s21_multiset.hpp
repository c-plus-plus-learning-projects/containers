#ifndef SRC_INCLUDE_S21_MULTISET_HPP_
#define SRC_INCLUDE_S21_MULTISET_HPP_

#include "RBTree.hpp"

namespace s21 {

template <typename T>
class multiset : public RBTree<T> {
 public:
        using iterator = typename RBTree<T>::iterator;

        multiset() : RBTree<T>() {}
        multiset(std::initializer_list<T> const &items) : RBTree<T>(items) {}
        multiset(const multiset &other) : RBTree<T>(other) {}
        multiset(multiset &&other) : RBTree<T>(other) {}

        multiset<T> &operator=(const multiset<T> &other) {
            RBTree<T>::operator=(other);
            return *this;
        }

        multiset<T> &operator=(multiset<T> &&other) {
            RBTree<T>::operator=(other);
            return *this;
        }

        template <class... Args>
        iterator emplace(Args &&...args) {
            return (RBTree<T>::insert(args), ...);
        }

        std::pair<iterator, bool> emplace(void) = delete;
};
}  // namespace s21

#endif  // SRC_INCLUDE_S21_MULTISET_HPP_
