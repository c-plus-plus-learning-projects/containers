#ifndef SRC_INCLUDE_S21_STACK_HPP_
#define SRC_INCLUDE_S21_STACK_HPP_

#include "s21_quene_core.hpp"

namespace s21 {

template <typename T>
class stack : public quene_core<T> {
 public:
    using value_type = T;
    using reference = T &;
    using const_reference = const T &;
    using size_type = size_t;
    stack() : quene_core<T>() {}
    explicit stack(std::initializer_list<value_type> const &items);
    explicit stack(const stack &s);
    explicit stack(stack &&s);

    void push(const_reference value) override;

    const_reference top() const { return quene_core<T>::head->data; }
    reference top() { return quene_core<T>::head->data; }
    stack &operator=(stack &&s);
    template <typename... Args>
    void emplace_front(Args &&...args);
};

    template <typename T>
    stack<T>::stack(const stack &s) : quene_core<T>() {
        typename s21::quene_core<T>::node *tmp = s.tail;
        if (tmp != nullptr) {
            while (tmp != nullptr) {
                stack<T>::push(tmp->data);
                tmp = tmp->next;
            }
        }
    }

    template <typename T>
    stack<T>::stack(stack &&s) : quene_core<T>() {
        quene_core<T>::move(std::move(s));
    }

    template <typename T>
    stack<T>::stack(std::initializer_list<value_type> const &items) : quene_core<T>() {
        quene_core<T>::quene_core_init(items);
    }

    template <typename T>
    void stack<T>::push(const_reference value) {
        quene_core<T>::create_top_node();
        quene_core<T>::head->data = value;
    }

    template <typename T>
    stack<T> &stack<T>::operator=(stack &&s) {
        if (this != &s) {
            quene_core<T>::move(std::move(s));
        }
        return *this;
    }

    template <typename T>
    template <typename... Args>
    void stack<T>::emplace_front(Args &&...args) {
        (push(args), ...);
    }

}  //  namespace s21

#endif  // SRC_INCLUDE_S21_STACK_HPP_
