#ifndef SRC_INCLUDE_S21_QUEUE_HPP_
#define SRC_INCLUDE_S21_QUEUE_HPP_

#include "s21_quene_core.hpp"

namespace s21 {

template <typename T>
class queue : public quene_core<T> {
 public:
    using value_type = T;
    using reference = T &;
    using const_reference = const T &;
    using size_type = size_t;
    queue() : quene_core<T>() {}
    explicit queue(std::initializer_list<value_type> const &items);
    explicit queue(const queue &s);
    explicit queue(queue &&s);

    const_reference front() const { return quene_core<T>::head->data; }
    const_reference back() const { return quene_core<T>::tail->data; }
    reference front() { return quene_core<T>::head->data; }
    reference back() { return quene_core<T>::tail->data; }
    void push(const_reference value) override;
    queue<T> &operator=(queue &&s);

    template <typename... Args>
    void emplace_back(Args &&...args);
};

    template <typename T>
    void queue<T>::push(const_reference value) {
        quene_core<T>::create_tail_node();
        quene_core<T>::tail->data = value;
    }

    template <typename T>
    queue<T>::queue(const queue &s) : quene_core<T>() {
        typename s21::quene_core<T>::node *tmp = s.head;
        if (tmp != nullptr) {
            while (tmp != nullptr) {
                queue<T>::push(tmp->data);
                tmp = tmp->prev;
            }
        }
    }

    template <typename T>
    queue<T>::queue(queue &&s) : quene_core<T>() {
        quene_core<T>::move(std::move(s));
    }

    template <typename T>
    queue<T>::queue(std::initializer_list<value_type> const &items) : quene_core<T>() {
        quene_core<T>::quene_core_init(items);
    }

    template <typename T>
    queue<T> &queue<T>::operator=(queue &&s) {
        if (this != &s) {
            quene_core<T>::move(std::move(s));
        }
        return *this;
    }

    template <typename T>
    template <typename... Args>
    void queue<T>::emplace_back(Args &&...args) {
        (push(args), ...);
    }

}  // namespace s21

#endif  // SRC_INCLUDE_S21_QUEUE_HPP_
