#include "s21_list.hpp"

#include <gtest/gtest.h>

#include <list>
#include <string>

template <typename T>
void print_list(const T& list) {
    auto tmp_l = list;
    for (auto i = tmp_l.begin(); i != tmp_l.end(); i++) {
        if (i != tmp_l.begin())
            std::cout << ":";
        std::cout << *i;
    }
    std::cout << std::endl;
}

TEST(size, LIST) {
    s21::list<int> s21{1, 2, 3, 4, 5};
    EXPECT_EQ(true, s21.size() == 5);
}

TEST(list_free, LIST) {
    s21::list<int> s21;
    int ii = 0;
    for (auto i = s21.begin(); i != s21.end(); i++) {
        ii++;
    }
    EXPECT_EQ(true, ii == 0);
}

TEST(iter_increment, LIST) {
    s21::list<int> s21{1, 2, 3, 4, 5};
    auto a = s21.begin();
    auto b = s21.end();
    *(s21.begin()) = 21;
    a++;
    ++b;
    ++b;
    EXPECT_EQ(true, *a == 2);
    EXPECT_EQ(true, *b == 2);
    EXPECT_EQ(true, *(s21.begin()) == 21);
}

TEST(iter_decrement, LIST) {
    s21::list<int> s21{1, 2, 3, 4, 5};
    auto a = s21.begin();
    auto b = s21.end();
    --a;
    --a;
    b--;
    b--;
    EXPECT_EQ(true, *a == 5);
    EXPECT_EQ(true, *b == 4);
}

TEST(insert, LIST) {
    s21::list<int> s21{2, 4, 16, 11, 3};
    s21.insert(s21.begin(), 21);
    EXPECT_EQ(true, *s21.begin() == 21);
}

TEST(insert2, LIST) {
    s21::list<int> s21{2, 4, 16, 11, 3};
    auto iter = s21.insert(s21.end(), 21);
    EXPECT_EQ(true, *--s21.end() == 21);
    EXPECT_EQ(true, *iter == 21);
}

TEST(insert3, LIST) {
    s21::list<int> s21{2, 4, 16, 11, 3};
    s21.insert(++s21.begin(), 21);
    EXPECT_EQ(true, *++s21.begin() == 21);
}

TEST(insert4, LIST) {
    s21::list<int> s21{2, 4, 16, 11, 3};
    s21.insert(--s21.end(), 21);
    EXPECT_EQ(true, *-- --s21.end() == 21);
}

TEST(push_back, LIST) {
    s21::list<int> s21{2, 4, 16, 11, 3};
    s21.push_back(21);
    EXPECT_EQ(true, *--s21.end() == 21);
}

TEST(push_back2, LIST) {
    s21::list<int> s21;
    s21.push_back(21);
    EXPECT_EQ(true, *--s21.end() == 21);
}

TEST(push_back3, LIST) {
    s21::list<std::string> s21;
    EXPECT_NO_THROW(s21.push_back("21"));
}

TEST(copy, LIST) {
    s21::list<int> s21{1, 2, 3, 4, 5};
    s21::list<int> copy = s21;
    EXPECT_EQ(true, copy.size() == 5);
    EXPECT_EQ(true, *(copy.begin()) == 1);
    EXPECT_EQ(true, *(--copy.end()) == 5);
}

TEST(move, LIST) {
    s21::list<int> b(s21::list<int>{1, 2, 3, 4, 5});
    auto member = std::move(b);
    EXPECT_EQ(true, member.size() == 5);
    EXPECT_EQ(true, *(member.begin()) == 1);
    EXPECT_EQ(true, *(--member.end()) == 5);
}

TEST(operator_eq, LIST) {
    s21::list<int> a{3, 7};
    s21::list<int> b{1, 2, 4, 16, 5};
    a = b;
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 1);
    iter++;
    EXPECT_EQ(true, *iter == 2);
    iter++;
    EXPECT_EQ(true, *iter == 4);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, *iter == 5);
    iter++;
}

TEST(operator_assign, LIST) {
    s21::list<int> a{3, 7};
    a = s21::list<int>{1, 2, 4, 16, 5};
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 1);
    iter++;
    EXPECT_EQ(true, *iter == 2);
    iter++;
    EXPECT_EQ(true, *iter == 4);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, *iter == 5);
    iter++;
}

TEST(front_back, LIST) {
    s21::list<int> mylist;
    mylist.push_back(77);
    mylist.push_back(22);
    mylist.front() -= mylist.back();
    EXPECT_EQ(true, mylist.front() == 55);
}

TEST(empty, LIST) {
    s21::list<int> mylist;
    EXPECT_EQ(true, mylist.empty());
}

TEST(max_size, LIST) {
    std::list<int> a{78, 16, 21};
    std::list<double> a2{7, 1};
    s21::list<int> b{78, 16, 21};
    s21::list<double> b2{7, 1};
    EXPECT_EQ(true, a.max_size() == b.max_size());
    EXPECT_EQ(true, a2.max_size() == b2.max_size());
}

TEST(erase, LIST) {
    s21::list<int> a{78, 16, 21};
    a.erase(++a.begin());
    EXPECT_EQ(true, *a.begin() == 78);
    EXPECT_EQ(true, *(++a.begin()) == 21);
    EXPECT_EQ(true, a.size() == 2);
}

TEST(pop_back, LIST) {
    s21::list<int> a{78, 16, 21};
    a.pop_back();
    EXPECT_EQ(true, *a.begin() == 78);
    EXPECT_EQ(true, *(++a.begin()) == 16);
    EXPECT_EQ(true, a.size() == 2);
}

TEST(push_front, LIST) {
    s21::list<int> a{78, 16, 21};
    a.push_front(99);
    EXPECT_EQ(true, *a.begin() == 99);
    EXPECT_EQ(true, *(++a.begin()) == 78);
    EXPECT_EQ(true, *(++ ++a.begin()) == 16);
    EXPECT_EQ(true, *(++ ++ ++a.begin()) == 21);
    EXPECT_EQ(true, a.size() == 4);
}

TEST(pop_front, LIST) {
    s21::list<int> a{78, 16, 21};
    a.pop_front();
    EXPECT_EQ(true, *a.begin() == 16);
    EXPECT_EQ(true, *(++a.begin()) == 21);
    EXPECT_EQ(true, a.size() == 2);
}

TEST(swap, LIST) {
    s21::list<int> a{78, 16};
    s21::list<int> b{1, 3, 9};
    a.swap(b);
    EXPECT_EQ(true, *a.begin() == 1);
    EXPECT_EQ(true, *(++a.begin()) == 3);
    EXPECT_EQ(true, *(++ ++a.begin()) == 9);
    EXPECT_EQ(true, a.size() == 3);
    EXPECT_EQ(true, *b.begin() == 78);
    EXPECT_EQ(true, *(++b.begin()) == 16);
    EXPECT_EQ(true, b.size() == 2);
}

TEST(sort, LIST) {
    s21::list<int> a{78, 16, 21};
    a.sort();
    EXPECT_EQ(true, *a.begin() == 16);
    EXPECT_EQ(true, *(++a.begin()) == 21);
    EXPECT_EQ(true, *(++ ++a.begin()) == 78);
    EXPECT_EQ(true, a.size() == 3);
}

TEST(sort2, LIST) {
    s21::list<int> a;
    EXPECT_NO_THROW(a.sort(););
}

TEST(sort3, LIST) {
    s21::list<int> a{78, 16};
    a.sort();
    EXPECT_EQ(true, *a.begin() == 16);
    EXPECT_EQ(true, *(++a.begin()) == 78);
    EXPECT_EQ(true, a.size() == 2);
}

TEST(sort4, LIST) {
    s21::list<int> a{78};
    a.sort();
    EXPECT_EQ(true, *a.begin() == 78);
    EXPECT_EQ(true, a.size() == 1);
}

TEST(merge, LIST) {
    s21::list<int> a{16, 21, 78};
    s21::list<int> b{2, 4, 6};
    a.merge(b);
    auto iter = a.begin();
    std::cout << *iter << std::endl;
    EXPECT_EQ(true, *iter == 2);
    iter++;
    EXPECT_EQ(true, *iter == 4);
    iter++;
    EXPECT_EQ(true, *iter == 6);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, *iter == 21);
    iter++;
    EXPECT_EQ(true, *iter == 78);
    iter++;
    EXPECT_EQ(true, a.size() == 6);
}

TEST(merge2, LIST) {
    s21::list<int> a{16, 21};
    s21::list<int> b{2, 4, 6};
    a.merge(b);
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 2);
    iter++;
    EXPECT_EQ(true, *iter == 4);
    iter++;
    EXPECT_EQ(true, *iter == 6);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, *iter == 21);
    iter++;
    EXPECT_EQ(true, a.size() == 5);
}

TEST(merge3, LIST) {
    s21::list<int> a{16, 21, 78};
    s21::list<int> b{2, 4};
    a.merge(b);
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 2);
    iter++;
    EXPECT_EQ(true, *iter == 4);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, *iter == 21);
    iter++;
    EXPECT_EQ(true, *iter == 78);
    iter++;
    EXPECT_EQ(true, a.size() == 5);
}

TEST(splice, LIST) {
    s21::list<int> list1 = {1, 2, 3, 4, 5};
    s21::list<int> list2 = {10, 20, 30, 40, 50};
    auto const& const_instance = list1;
    auto it2 = s21::list<int>::const_iterator(const_instance.begin().get_node()->_next);
    list1.splice(it2, list2);

    auto iter = list1.begin();
    EXPECT_EQ(true, *iter == 1);
    iter++;
    EXPECT_EQ(true, *iter == 2);
    iter++;
    EXPECT_EQ(true, *iter == 10);
    iter++;
    EXPECT_EQ(true, *iter == 20);
    iter++;
    EXPECT_EQ(true, *iter == 30);
    iter++;
    EXPECT_EQ(true, *iter == 40);
    iter++;
    EXPECT_EQ(true, *iter == 50);
    iter++;
    EXPECT_EQ(true, *iter == 3);
    iter++;
    EXPECT_EQ(true, *iter == 4);
    iter++;
    EXPECT_EQ(true, *iter == 5);
    iter++;
    EXPECT_EQ(true, list1.size() == 10);

    EXPECT_EQ(true, list2.size() == 0);
}

TEST(reverse, LIST) {
    s21::list<int> a{16, 21, 78, 15};
    a.reverse();
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 15);
    iter++;
    EXPECT_EQ(true, *iter == 78);
    iter++;
    EXPECT_EQ(true, *iter == 21);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, a.size() == 4);
}

TEST(unique, LIST) {
    s21::list<int> a{1, 2, 2, 2, 3, 4, 4, 5};
    a.unique();
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 1);
    iter++;
    EXPECT_EQ(true, *iter == 2);
    iter++;
    EXPECT_EQ(true, *iter == 3);
    iter++;
    EXPECT_EQ(true, *iter == 4);
    iter++;
    EXPECT_EQ(true, *iter == 5);
    iter++;
    EXPECT_EQ(true, a.size() == 5);
}

TEST(iter_eq, LIST) {
    s21::list<int> a{1, 2, 2, 2, 3, 4, 4, 5};
    auto iter_b = a.begin();
    auto iter_e = a.end();
    EXPECT_EQ(false, iter_b == iter_e);
}

TEST(iter_free, LIST) {
    s21::list<int> a{1, 2, 2, 2, 3, 4, 4, 5};
    EXPECT_NO_THROW(s21::list<int>::iterator());
}

TEST(emplace_back0, LIST) {
    s21::list<int> a{78, 16, 21};
    a.emplace_back();
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 78);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, *iter == 21);
    iter++;
    EXPECT_EQ(true, a.size() == 3);
}

TEST(emplace_back1, LIST) {
    s21::list<int> a{78, 16, 21};
    a.emplace_back(77);
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 78);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, *iter == 21);
    iter++;
    EXPECT_EQ(true, *iter == 77);
    iter++;
    EXPECT_EQ(true, a.size() == 4);
}

TEST(emplace_back3, LIST) {
    s21::list<int> a{78, 16, 21};
    a.emplace_back(3, 9, 27);
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 78);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, *iter == 21);
    iter++;
    EXPECT_EQ(true, *iter == 3);
    iter++;
    EXPECT_EQ(true, *iter == 9);
    iter++;
    EXPECT_EQ(true, *iter == 27);
    iter++;
    EXPECT_EQ(true, a.size() == 6);
}

TEST(emplace_back5, LIST) {
    s21::list<int> a{78, 16, 21};
    a.emplace_back(3, 9, 27, 144, 256);
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 78);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, *iter == 21);
    iter++;
    EXPECT_EQ(true, *iter == 3);
    iter++;
    EXPECT_EQ(true, *iter == 9);
    iter++;
    EXPECT_EQ(true, *iter == 27);
    iter++;
    EXPECT_EQ(true, *iter == 144);
    iter++;
    EXPECT_EQ(true, *iter == 256);
    iter++;
    EXPECT_EQ(true, a.size() == 8);
}

TEST(emplace_front0, LIST) {
    s21::list<int> a{78, 16, 21};
    a.emplace_front();
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 78);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, *iter == 21);
    iter++;
    EXPECT_EQ(true, a.size() == 3);
}

TEST(emplace_front1, LIST) {
    s21::list<int> a{78, 16, 21};
    a.emplace_front(77);
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 77);
    iter++;
    EXPECT_EQ(true, *iter == 78);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, *iter == 21);
    iter++;
    EXPECT_EQ(true, a.size() == 4);
}

TEST(emplace_front3, LIST) {
    s21::list<int> a{78, 16, 21};
    a.emplace_front(3, 9, 27);
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 3);
    iter++;
    EXPECT_EQ(true, *iter == 9);
    iter++;
    EXPECT_EQ(true, *iter == 27);
    iter++;
    EXPECT_EQ(true, *iter == 78);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, *iter == 21);
    iter++;
    EXPECT_EQ(true, a.size() == 6);
}

TEST(emplace_front5, LIST) {
    s21::list<int> a{78, 16, 21};
    a.emplace_front(3, 9, 27, 144, 256);
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 3);
    iter++;
    EXPECT_EQ(true, *iter == 9);
    iter++;
    EXPECT_EQ(true, *iter == 27);
    iter++;
    EXPECT_EQ(true, *iter == 144);
    iter++;
    EXPECT_EQ(true, *iter == 256);
    iter++;
    EXPECT_EQ(true, *iter == 78);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, *iter == 21);
    iter++;
    EXPECT_EQ(true, a.size() == 8);
}

TEST(emplace3, LIST) {
    s21::list<int> a{78, 16, 21};
    auto const& const_instance = a;
    auto it2 = s21::list<int>::const_iterator(const_instance.begin().get_node()->_next);
    a.emplace(it2, 77, 44, 43);
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 78);
    iter++;
    EXPECT_EQ(true, *iter == 77);
    iter++;
    EXPECT_EQ(true, *iter == 44);
    iter++;
    EXPECT_EQ(true, *iter == 43);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, *iter == 21);
    iter++;
    EXPECT_EQ(true, a.size() == 6);
}

TEST(emplace0, LIST) {
    s21::list<int> a{78, 16, 21};
    auto const& const_instance = a;
    a.emplace(const_instance.begin());
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 78);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, *iter == 21);
    iter++;
    EXPECT_EQ(true, a.size() == 3);
}

TEST(emplace5, LIST) {
    s21::list<int> a{78, 16, 21};
    auto const& const_instance = a;
    auto it2 = s21::list<int>::const_iterator(const_instance.begin().get_node()->_next);
    a.emplace(it2, 77, 44, 43, 11, 13);
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 78);
    iter++;
    EXPECT_EQ(true, *iter == 77);
    iter++;
    EXPECT_EQ(true, *iter == 44);
    iter++;
    EXPECT_EQ(true, *iter == 43);
    iter++;
    EXPECT_EQ(true, *iter == 11);
    iter++;
    EXPECT_EQ(true, *iter == 13);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, *iter == 21);
    iter++;
    EXPECT_EQ(true, a.size() == 8);
}

TEST(emplace1, LIST) {
    s21::list<int> a{78, 16, 21};
    auto const& const_instance = a;
    auto it2 = s21::list<int>::const_iterator(const_instance.begin().get_node()->_next);
    a.emplace(it2, 77);
    auto iter = a.begin();
    EXPECT_EQ(true, *iter == 78);
    iter++;
    EXPECT_EQ(true, *iter == 77);
    iter++;
    EXPECT_EQ(true, *iter == 16);
    iter++;
    EXPECT_EQ(true, *iter == 21);
    iter++;
    EXPECT_EQ(true, a.size() == 4);
}
