#include "s21_vector.hpp"

#include <gtest/gtest.h>

#include <vector>

template <typename T>
bool vector_cmp(s21::vector<T> s21_, std::vector<T> std_) {
    bool res = s21_.size() == std_.size();
    if (res)
        res = s21_.capacity() == std_.capacity();
    for (auto i = std_.begin(), j = s21_.begin(); i < std_.end() && res; i++, j++) {
        res = *i == *j;
    }
    return res;
}

TEST(end2, VECTOR) {
    std::vector<int> a{1, 2, 3, 4, 5};
    s21::vector<int> b{1, 2, 3, 4, 5};
    EXPECT_EQ(true, *(a.end() - 1) == *(b.end() - 1));
}

TEST(insert, VECTOR) {
    std::vector<int> a{1, 2, 3, 4, 5};
    s21::vector<int> b{1, 2, 3, 4, 5};
    EXPECT_EQ(true, *(a.insert(a.end(), 21)) == *(b.insert(b.end(), 21)));
    EXPECT_EQ(true, vector_cmp(b, a));
}

TEST(insert2, VECTOR) {
    std::vector<int> a{1, 2, 3, 4, 5};
    s21::vector<int> b{1, 2, 3, 4, 5};
    EXPECT_EQ(true, *(a.insert(a.begin(), 21)) == *(b.insert(b.begin(), 21)));
    EXPECT_EQ(true, vector_cmp(b, a));
}

TEST(create_vector_free, VECTOR) {
    std::vector<int> a;
    s21::vector<int> b;
    EXPECT_EQ(true, vector_cmp(b, a));
}

TEST(create_vector_n, VECTOR) {
    std::vector<int> a(5);
    s21::vector<int> b(5);
    EXPECT_EQ(true, vector_cmp(b, a));
}

TEST(create_vector_n_fail, VECTOR) {
    EXPECT_ANY_THROW(s21::vector<int> b(-1));
}

TEST(create_vector_list, VECTOR) {
    std::initializer_list<int> items{1, 2, 3, 4, 5};
    std::vector<int> a(items);
    s21::vector<int> b(items);
    EXPECT_EQ(true, vector_cmp(b, a));
}

TEST(create_vector_copy, VECTOR) {
    std::vector<int> a{1, 2, 3, 4, 5};
    s21::vector<int> b{1, 2, 3, 4, 5};
    std::vector<int> a2(a);
    s21::vector<int> b2(b);
    EXPECT_EQ(true, vector_cmp(b2, a2));
}

TEST(create_vector_move, VECTOR) {
    std::vector<int> a(std::vector<int>{1, 2, 3, 4, 5});
    auto member = std::move(a);
    s21::vector<int> b(s21::vector<int>{1, 2, 3, 4, 5});
    auto member2 = std::move(b);
    EXPECT_EQ(true, vector_cmp(member2, member));
}

TEST(create_vector_op_assign, VECTOR) {
    std::vector<int> a{1, 2, 3, 4, 5};
    s21::vector<int> b{1, 2, 3, 4, 5};
    std::vector<int> a2 = a;
    s21::vector<int> b2 = b;
    EXPECT_EQ(true, vector_cmp(b2, a2));
}

TEST(create_vector_op_assign2, VECTOR) {
    std::vector<int> a{1, 2, 3, 4, 5};
    std::vector<int> b{1, 2, 3};
    s21::vector<int> a21{1, 2, 3, 4, 5};
    s21::vector<int> b21{1, 2, 3};
    a = b;
    a21 = b21;
    EXPECT_EQ(true, vector_cmp(a21, a));
}

TEST(create_vector_op_assign3, VECTOR) {
    std::vector<int> a{1, 2, 3, 4, 5};
    std::vector<int> b{1, 2, 3};
    s21::vector<int> a21{1, 2, 3, 4, 5};
    s21::vector<int> b21{1, 2, 3};
    b = a;
    b21 = a21;
    EXPECT_EQ(true, vector_cmp(b21, b));
}

TEST(at1, VECTOR) {
    std::vector<int> a{1, 2, 3, 4, 5};
    s21::vector<int> b{1, 2, 3, 4, 5};
    EXPECT_EQ(true, a.at(2) == b.at(2));
}

TEST(at2, VECTOR) {
    std::vector<int> a{1, 2, 3, 4, 5};
    s21::vector<int> b{1, 2, 3, 4, 5};
    a.at(3) = 21;
    b.at(3) = 21;
    EXPECT_EQ(true, a.at(3) == b.at(3));
}

TEST(oper_at, VECTOR) {
    std::vector<int> a{1, 2, 3, 4, 5};
    s21::vector<int> b{1, 2, 3, 4, 5};
    a[3] = 21;
    b[3] = 21;
    EXPECT_EQ(true, a[3] == b[3]);
}

TEST(front_back, VECTOR) {
    std::vector<int> a{78, 16};
    s21::vector<int> b{78, 16};
    a.front() -= a.back();
    b.front() -= b.back();
    EXPECT_EQ(true, a.front() == b.front());
}

TEST(data, VECTOR) {
    std::vector<int> a{78, 16};
    s21::vector<int> b{78, 16};
    EXPECT_EQ(true, a.data() != b.data());
    EXPECT_EQ(true, *a.data() == *b.data());
}

TEST(empty_false, VECTOR) {
    std::vector<int> a{78, 16};
    s21::vector<int> b{78, 16};
    EXPECT_EQ(true, a.empty() == b.empty());
}

TEST(empty_true, VECTOR) {
    std::vector<int> a;
    s21::vector<int> b;
    EXPECT_EQ(true, a.empty() == b.empty());
}

TEST(reserve, VECTOR) {
    std::vector<int> a{78, 16};
    s21::vector<int> b{78, 16};
    a.reserve(5);
    b.reserve(5);
    EXPECT_EQ(true, vector_cmp(b, a));
    a.reserve(5);
    b.reserve(5);
    EXPECT_EQ(true, vector_cmp(b, a));
    a.reserve(1);
    b.reserve(1);
    EXPECT_EQ(true, vector_cmp(b, a));
}

TEST(shrink_to_fit, VECTOR) {
    std::vector<int> a{78, 16};
    s21::vector<int> b{78, 16};
    a.pop_back(); a.shrink_to_fit();
    b.pop_back(); b.shrink_to_fit();
    EXPECT_EQ(true, vector_cmp(b, a));
}

TEST(clear, VECTOR) {
    std::vector<int> a{78, 16};
    s21::vector<int> b{78, 16};
    a.clear();
    b.clear();
    EXPECT_EQ(true, vector_cmp(b, a));
}

TEST(erase, VECTOR) {
    std::vector<int> a{78, 16, 21};
    s21::vector<int> b{78, 16, 21};
    a.erase(a.begin() + 1);
    b.erase(b.begin() + 1);
    EXPECT_EQ(true, vector_cmp(b, a));
}

TEST(swap, VECTOR) {
    std::vector<int> a{78, 16, 21};
    std::vector<int> a2{7, 1};
    s21::vector<int> b{78, 16, 21};
    s21::vector<int> b2{7, 1};
    a.swap(a2);
    b.swap(b2);
    EXPECT_EQ(true, vector_cmp(b, a));
}

TEST(max_size, VECTOR) {
    std::vector<int> a{78, 16, 21};
    std::vector<double> a2{7, 1};
    s21::vector<int> b{78, 16, 21};
    s21::vector<double> b2{7, 1};
    EXPECT_EQ(true, a.max_size() == b.max_size());
    EXPECT_EQ(true, a2.max_size() == b2.max_size());
}

TEST(emplace0, VECTOR) {
    s21::vector<int> b{78, 16, 21};
    std::vector<int> c{78, 16, 21};
    b.emplace(b.begin());
    EXPECT_EQ(true, vector_cmp(b, c));
}

TEST(emplace1, VECTOR) {
    s21::vector<int> b{78, 16, 21};
    std::vector<int> c{77, 78, 16, 21};
    b.emplace(b.begin(), 77);
    EXPECT_EQ(true, vector_cmp(b, c));
}

TEST(emplace3, VECTOR) {
    s21::vector<int> b{78, 16, 21};
    std::vector<int> c{77, 15, 13, 78, 16, 21};
    b.emplace(b.begin(), 77, 15, 13);
    EXPECT_EQ(true, vector_cmp(b, c));
    ;
}

TEST(emplace5, VECTOR) {
    s21::vector<int> b{78, 16, 21};
    std::vector<int> c{77, 15, 13, 5, 3, 78, 16, 21};
    b.emplace(b.begin(), 77, 15, 13, 5, 3);
    EXPECT_EQ(true, vector_cmp(b, c));
}

TEST(emplace_back0, VECTOR) {
    s21::vector<int> b{78, 16, 21};
    std::vector<int> c{78, 16, 21};
    b.emplace_back();
    EXPECT_EQ(true, vector_cmp(b, c));
}

TEST(emplace_back1, VECTOR) {
    s21::vector<int> b{78, 16, 21};
    std::vector<int> c{78, 16, 21, 77};
    b.emplace_back(77);
    EXPECT_EQ(true, vector_cmp(b, c));
}

TEST(emplace_back3, VECTOR) {
    s21::vector<int> b{78, 16, 21};
    std::vector<int> c{78, 16, 21, 77, 15, 13};
    b.emplace_back(77, 15, 13);
    EXPECT_EQ(true, vector_cmp(b, c));
    ;
}

TEST(emplace_back5, VECTOR) {
    s21::vector<int> b{78, 16, 21};
    std::vector<int> c{78, 16, 21, 77, 15, 13, 5, 3};
    b.emplace_back(77, 15, 13, 5, 3);
    EXPECT_EQ(true, vector_cmp(b, c));
}

TEST(create_vector_op_assign4, VECTOR) {
    s21::vector<int> a21{1, 2, 3, 4, 5};
    a21 = s21::vector<int>{6, 7, 8};
    EXPECT_EQ(true, *(a21.begin()) == 6);
    EXPECT_EQ(true, *(a21.begin() + 1) == 7);
    EXPECT_EQ(true, *(a21.begin() + 2) == 8);
}
